<?php

class StartCycle
{
    public function __construct()
    {
        // Required dependencies / helpers
        foreach (glob("Inc/*.php") as $filename) {
            require_once($filename);
        }

        // Lets start our day
        $sun = new Sun();
        $sun->initiateCycle();
    }
} ?>