<?php

class Flowers
{
    /**
     * @var $sun
     */
    public $sun;

    /**
     * @var $state
     */
    public $time;

    /**
     * @var $state
     */
    public $state;

    /**
     * @var
     */
    public $sugarbird;

    /**
     * @var array $dryFlowers
     * Default: Set to dry
     */
    public $dryFlowers = [
        'Dahlia'   => 0,
        'Lily'     => 0,
        'Jasmine'  => 0,
        'Zinnia'   => 0,
        'Lotus'    => 0,
        'Camellia' => 0,
        'Primrose' => 0,
        'Daisy'    => 0,
        'Tulip'    => 0,
        'Poppy'    => 0,
        'Holly'    => 0,
        'Daffodil' => 0,
    ];

    /**
     * @var array $flowersUnique
     * Default: Set to active
     */
    private $flowersUnique = [
        'Dahlia'   => 1,
        'Lily'     => 1,
        'Jasmine'  => 1,
        'Zinnia'   => 1,
        'Lotus'    => 1,
        'Camellia' => 1,
        'Primrose' => 1,
        'Daisy'    => 1,
        'Tulip'    => 1,
        'Poppy'    => 1,
        'Holly'    => 1,
        'Daffodil' => 1,
    ];

    /**
     * @var $flowers
     */
    public $flowersFedCount;

    public function __construct()
    {
        // Specify our start flower feed count (Set by default)
        $this->flowersFedCount = [
            'Dahlia'   => 10,
            'Lily'     => 10,
            'Jasmine'  => 10,
            'Zinnia'   => 10,
            'Lotus'    => 10,
            'Camellia' => 10,
            'Primrose' => 10,
            'Daisy'    => 10,
            'Tulip'    => 10,
            'Poppy'    => 10,
            'Holly'    => 10,
            'Daffodil' => 10,
        ];
    }

    public function setFlowerUnique($input)
    {
        $this->flowersUnique = $input;
    }

    public function getFlowerUnique()
    {
        return $this->flowersUnique;
    }

    public function produceNectar()
    {
        // Lets create new instance of Sugarbird class
        $this->sugarbird = new Sugarbird;

        // Lets create new instance of our Sun class to utilise our events
        $this->sun = new Sun;

        //Lets start our day
        $this->state = $this->sun->onDayStart();

        // Setting time of day to noon by default
        $date = new DateTime();
        $date->setTime(12, 00, 00);
        $this->time = $date->format('H:i:s');

        // Lets start our foreach day cycle
        for ($i = 1; $i <= 228; $i++) {
            // Lets check if our time is noon
            if ($this->time === '12:00:00') {
                //Start our new day
                $this->state = $this->sun->onDayStart();

                // Set all our flowers to have produced nectar over the period from mid night to noon
                $this->setFlowerUnique([
                    'Dahlia'   => 1,
                    'Lily'     => 1,
                    'Jasmine'  => 1,
                    'Zinnia'   => 1,
                    'Lotus'    => 1,
                    'Camellia' => 1,
                    'Primrose' => 1,
                    'Daisy'    => 1,
                    'Tulip'    => 1,
                    'Poppy'    => 1,
                    'Holly'    => 1,
                    'Daffodil' => 1,
                ]);
            }

            // Increment our time by 1 hour everytime we iterate through foreach
            $this->time = date('H:i:s', strtotime($this->time . '+ 1 hour'));

            if ($this->state) {
                $availableFlowerValue = [1];
                $scalars = array_filter($this->getFlowerUnique(), function ($item) { return !is_array($item); });
                $foundAvailableFlowers = array_intersect($scalars, $availableFlowerValue);
                if (!empty($foundAvailableFlowers)) {
                    $randPickedFlower = array_rand($foundAvailableFlowers, 1);

                    // We fed on a flower, so lets keep track of fed count
                    $this->flowersFedCount[$randPickedFlower] = $this->sugarbird->feed($this->flowersFedCount[$randPickedFlower]);

                    // Lets output our feeding notification
                    $this->sugarbird->feedNotification($this->time, $i, $randPickedFlower, $this->flowersFedCount[$randPickedFlower]);

                    // Extra line break to neaten up things
                    echo PHP_EOL;

                    // Set flower to have been fed on
                    $this->flowersUnique[$randPickedFlower] = 0;

                } else {
                    // All flowers have been emptied of nectar within the 12 hour period
                    $this->state = $this->sun->onDayEnd();
                }
            } else {
                printf('Day complete and all flowers have been emptied of nectar. Sleeping... HOUR CHANGE - ' . $this->time . ' - ' . $i . PHP_EOL);
            }

            // Lets end our application when all values in array are the same which is specified as 0
            if ($this->dryFlowers == $this->flowersFedCount) {
                printf('Our application has completed and all flowers emptied of their nectar.');
                exit;
            }

        }
    }

}