<?php

class Sugarbird
{
    public function __construct()
    {
        // Silence is Golden
    }

    public function feed($flowerFedCount)
    {
        return $flowerFedCount - 1;
    }

    public function feedNotification($time, $loopCount, $flowerName, $flowerFedCount)
    {
        // Specify the hour in time and count
        printf('HOUR CHANGE ' . $time . ' - ' . $loopCount . PHP_EOL);

        // Specify the flower that has been fed on and it's remaining count
        printf('FLOWER - ' . $flowerName . '(' . $flowerFedCount . ')' . PHP_EOL);
    }
}