<?php

class Sun
{
    /**
     * @var $state
     */
    public $time;

    /**
     * @var $state
     */
    public $state;

    /**
     * @var $flowers
     */
    public $flowers;


    public function __construct()
    {
        // Silence is golden
    }

    /**
     * initiateCycle
     */
    public function initiateCycle()
    {
        $initiateFeeding = new Flowers();
        $initiateFeeding->produceNectar();
    }

    public function onDayStart()
    {
        return 1;
    }

    public function onDayEnd()
    {
        return 0;
    }

    public function onHourChange()
    {
        // Silence is golden
    }
}