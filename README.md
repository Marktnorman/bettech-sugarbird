## BetTech Sugarbird application

This is a small apllication assessmnet written for BetTech by Mark Norman

Website - http://marktnorman.com

## Requirements

Please be sure to have PHP on the machine running the test.

## Installation

### Get the code

clone the repo and install the dependancies

```terminal
git clone git@gitlab.com:Marktnorman/bettech-sugarbird.git
cd bettech-sugarbird
```

## Run via command line
```
php -a
```
The require your only dependencies
```
require_once "StartCycle.php";
```
Run our test
```
new StartCycle();
```